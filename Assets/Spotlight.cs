﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spotlight : MonoBehaviour {


    [SerializeField] float maxIntensity = 2.27f;
    [SerializeField] float offIntensity = 0.01f;
    private float lerpedIntensity;

    private Firefly firefly;
    [SerializeField] float lerpTime = 1.5f;
    public float currentLerpTime;

    bool isFlashing = false;


    // Use this for initialization
    void Start () {
        firefly = FindObjectOfType<Firefly>();
	}
	
	// Update is called once per frame
	void Update () {
        if (firefly.isBoosting)
        { FlashLight(); }
        else { SlowGlow(); }
	}

    private void FlashLight()
    {
        GetComponent<Light>().intensity = maxIntensity;
    }

    private void DimLight()
    {
        GetComponent<Light>().intensity = 0.1f;
    }

    private void SlowGlow()
    {
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = 0f;
            if (isFlashing)
            { isFlashing = false; }
            else { isFlashing = true; }
        }

        float perc = currentLerpTime / lerpTime;

        if (!isFlashing)
        {
            lerpedIntensity = Mathf.Lerp(offIntensity, maxIntensity, perc);

        }
        else if (isFlashing)
        {
            lerpedIntensity = Mathf.Lerp(maxIntensity, offIntensity, perc);
        }
        else { print("neither flashing"); }

        GetComponent<Light>().intensity = lerpedIntensity;
    }


}

