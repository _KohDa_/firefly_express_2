﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidControls : MonoBehaviour {


    public void Thrust(GameObject firefly)
    {
        Rigidbody fireFlyRigidBody = firefly.GetComponent<Rigidbody>();
        fireFlyRigidBody.AddRelativeForce(new Vector3(0f, 10f, 0f));
    }

}
