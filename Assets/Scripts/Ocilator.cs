﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ocilator : MonoBehaviour {

    [SerializeField] Vector3 movementVector = new Vector3(10f, 10f, 10f);
    [SerializeField] float period = 2f;

    [Range(0f, 1f)] float movementFactor;

    Vector3 startingPosition;
    bool isFirefly;

    //float at start
    public static bool gameStarted = false;


    // Use this for initialization
    void Start () {


        gameStarted = false;

        isFirefly = (gameObject.tag == "Hero") ? true : false;
        if(isFirefly)
        { startingPosition = SaveProgress.startPos; }
        else { startingPosition = transform.position; }
        

    }
	
	// Update is called once per frame
	void FixedUpdate () {

        //if this is the firefly, ocilate

        if (isFirefly) { StartOnly(); }
        GameStarted();

        Ocilate();

    }

    void Ocilate()
    {

        if (period <= Mathf.Epsilon) { return; }
        float cycles = Time.time / period; // grows continually from 0 

        const float tau = Mathf.PI * 2; //about 6.28
        float rawSinWave = Mathf.Sin(cycles * tau);

        movementFactor = rawSinWave / 2 + 0.5f;
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPosition + offset;
    }

    void GameStarted()
    {
        bool takeOff = FindObjectOfType<Firefly>().isBoosting;
        if (takeOff)
        { gameStarted = true;
        }

        if (LevelManager.state != LevelManager.State.Alive)
        { gameStarted = false; }
    }

    void StartOnly()
    {
        if (gameStarted || System.Math.Round(transform.position.x, 1) != System.Math.Round(startingPosition.x, 1)) 
        {
            Destroy(this); }
    }
}
