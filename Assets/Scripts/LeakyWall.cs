﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeakyWall : MonoBehaviour {

    private Firefly firefly;

    private void Start()
    {
        firefly = FindObjectOfType<Firefly>();
    }

    void OnParticleCollision(GameObject other)
    {
        string otherName = other.name;
        if(otherName == null)
        { throw new ArgumentNullException("is null"); }
        else if (otherName == firefly.name)
        {
            firefly.ManageHits(other);
        }
   }
}
