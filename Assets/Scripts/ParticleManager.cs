﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {

    public static ParticleManager particleManager;

    //[SerializeField] public ParticleSystem boostParticles;
    [SerializeField] public GameObject hitParticles;
    //[SerializeField] public GameObject splashParticles;
    [SerializeField] int hitParticleEmissions = 100;
    //[SerializeField] ParticleSystem deathParticles; //not using death particles but a bulb flash


    void Awake()
    {

    }



    public void HitObjectParticles(Vector3 pos, Quaternion rot, Transform parent)
    {
        ParticleSystem hitInstance = Instantiate(hitParticles.GetComponent<ParticleSystem>(), pos, rot) as ParticleSystem;
        hitInstance.Emit(hitParticleEmissions);
        hitInstance.transform.parent = this.transform; //child object to this object for destruction
        //DestroyParticleSystem(hitInstance);

    }

    public void DestroyParticleSystem(ParticleSystem particleSystem)
    {

        float startTime = particleSystem.main.startLifetime.constantMax;
        float duration = particleSystem.main.duration;
        float totalDuration = startTime + duration;
        Destroy(particleSystem, totalDuration);

    }





}
