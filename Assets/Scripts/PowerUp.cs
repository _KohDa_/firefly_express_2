﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    private Firefly firefly;

    [Range(0f, 1f)]
    [SerializeField]
    public float healthRecoveredPercentage = .5f;
    private int healthBoost;
    private Rigidbody sphere;
    [SerializeField] float spin = 10f;

    void Start () {
        sphere = this.gameObject.GetComponent<Rigidbody>();
        sphere.AddTorque(new Vector3(spin, spin, 0f), ForceMode.Acceleration);

        firefly = FindObjectOfType<Firefly>();
        healthBoost = (int) (firefly.maxHits * healthRecoveredPercentage);
	}

    void OnTriggerEnter(Collider other) 
    {
        bool heroTag = (other.gameObject.tag != null) ? true : false;
        
        if(heroTag)
        {
            int hitsAfterBoost = Firefly.hits - healthBoost;
            if (hitsAfterBoost > 0)
            {
                Firefly.hits = hitsAfterBoost; }
            else { Firefly.hits = 0; }

            SoundManager.instance.soundEffects.PlayOneShot(SoundManager.instance.powerUp);
        }

        FindObjectOfType<SaveProgress>().SaveSpot(gameObject.transform.position);

        Destroy(this.gameObject);

    }
}
