﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ContinueButton : MonoBehaviour {

    public GameObject continueButtonGameObject;
    //[SerializeField] TMP_ColorGradient offGradient;
    [SerializeField] Color offColour;
    [SerializeField] Color offTextColor;

    private Color colour;
    //public float buttonAlpha = 0.2f;


    void OnEnable () {

        int firstLevelIndex = 1;


        if (PlayerPrefs.GetInt("MaxLevel") <= firstLevelIndex)
        {
            SetButtonColour();
        }
    }

    public void SetButtonColour()
    {
        Button continueButton = continueButtonGameObject.gameObject.GetComponent<Button>();

        colour = continueButton.GetComponent<Image>().color;
        colour = offColour;
        //colour.a = buttonAlpha;
        continueButton.GetComponent<Image>().color = colour;
        continueButton.GetComponentInChildren<Text>().color = offTextColor;


        //TextMeshProUGUI thisTextMesh = continueButton.GetComponentInChildren<TextMeshProUGUI>();
        //thisTextMesh.colorGradientPreset = offGradient;
        continueButton.interactable = false;
    }

}
