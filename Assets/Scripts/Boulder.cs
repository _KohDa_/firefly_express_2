﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour
{

    /// <summary>
    /// This script is to create the boulder and drop it.
    /// the script shoudl wait until the boulder hits x target and then destroy the boulder object
    /// once destroyed, a new boulder object is created
    /// </summary>
    /// 

    private SoundManager soundManager;

    public GameObject boulderPrefab;
    public Rigidbody boulderRidgidBody;
    private Transform boulderStartPos;
    [SerializeField] Vector3 pushDirection = new Vector3(10f, 0f, 0f);


    [SerializeField] float releaseDelayTime = 7f;
    [SerializeField] float destroyTime = 10f;



    // Use this for initialization
    void Start()
    {
        boulderStartPos = transform;
        soundManager = FindObjectOfType<SoundManager>();
        
        InvokeRepeating("CreateBoulder", 0f, releaseDelayTime);
    }


    public void CreateBoulder()
    {
        GameObject boulderInstance;
        boulderInstance = Instantiate(boulderPrefab, boulderStartPos.position, boulderStartPos.rotation);
        Rigidbody boulderRb = boulderInstance.GetComponent<Rigidbody>();
        boulderRb.velocity = pushDirection;
        //soundManager.BoulderRelease();
        Destroy(boulderInstance, destroyTime);

    }

    private void CreateBoulder2()

    {
        Rigidbody boulderInstance;
        boulderInstance = Instantiate(boulderRidgidBody, boulderStartPos.position, boulderStartPos.rotation) as Rigidbody;
        GameObject boulderObject = boulderInstance.gameObject;

        boulderInstance.velocity = pushDirection;
        soundManager.BoulderRelease();
        Destroy(boulderObject, destroyTime);
    }

}
