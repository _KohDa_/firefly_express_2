﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public PlayerData PlayerData { get; private set; }


    private void OnEnable()
    {
        PlayerData = PlayerPersistence.LoadData();

    }

    private void OnDisable()
    {
        PlayerPersistence.SaveData(this);

    }



}
