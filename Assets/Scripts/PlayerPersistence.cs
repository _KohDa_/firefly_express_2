﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPersistence : MonoBehaviour {

    public static void SaveData(Player player)
    {
        //enter save data here
        //PlayerPrefs.SetFloat("MusicVolume", VolumeManager.musicVolume);
      

    }

    public static PlayerData LoadData()
    {
        string name = PlayerPrefs.GetString("Name");
        int health = PlayerPrefs.GetInt("Health");
        int maxLevel = PlayerPrefs.GetInt("MaxLevel", 0);
        float musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        float effectsVolume = PlayerPrefs.GetFloat("EffectsVolume");
        bool musicIsOn = PlayerPrefsX.GetBool("MusicIsOn", true);

        PlayerData playerData = new PlayerData()
        {
            Health = health,
            Name = name,
            MaxLevel = maxLevel,
            MusicVolume = musicVolume,
            EffectsVolume = effectsVolume,
            MusicIsOn = musicIsOn
        };

        return playerData;

    }

}
