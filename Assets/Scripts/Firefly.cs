﻿
//using FireFlyExpress;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.EventSystems;

public class Firefly : MonoBehaviour
{

    Rigidbody fireFlyRigidBody;
    [SerializeField] float pulseForce = 5f; 
    [SerializeField] float velocity = 10f; //vertical thrust
    [SerializeField] float rotationalThrust = 200f; 
    [SerializeField] float selfRightingSpeed = .5f; // Bigger for slower
    [SerializeField] int deathFlickers = 6;
    [SerializeField] float flickerSpeed = 0.1f;

    [SerializeField] ParticleSystem boostParticles;
    [SerializeField] ParticleSystem levelWonParticles;

    public int maxHits = 3;
    public static int hits;

    private ParticleManager particleManager;

    public bool isBoosting = false; //bool to try bring in button and spacekey input

    public enum Rotation { Static, Right, Left };
    public static Rotation rotation;



    //for flicker
    GameObject fireFlyButt;
    MeshRenderer fireFlyLight;


    void Start () {

        GetVariables();

        gameObject.transform.position = SaveProgress.startPos;
    }

    private void GetVariables()
    {
        LevelManager.state = LevelManager.State.Alive;
        hits = 0;
        fireFlyRigidBody = GetComponent<Rigidbody>();


        //for flickering light of death
        fireFlyButt = GameObject.FindWithTag("FireFlyButt");
        fireFlyLight = fireFlyButt.GetComponent<MeshRenderer>();


        particleManager = FindObjectOfType<ParticleManager>();

        rotation = Rotation.Static;
    }


    // Update is called once per frame
    void Update ()
    {

        if (LevelManager.state == LevelManager.State.Alive)
        {
            ProcessInput();
            CheckIfDying();
        }

        RightFirefly(rotation);
    }

    private void FixedUpdate()
    {
        //position sometimes slips off the Z axis after loading and coliision
        //transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Mathf.Lerp(transform.localPosition.z, 0, Time.deltaTime * 1));
    //    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y,0);
   }

    private void ProcessInput()
    {

            RespondToThrust();
            RotationDirection();
            SkipLevel();
            RespondToRotate();
    }





    private void OnCollisionEnter(Collision collision) 
    {
        if (LevelManager.state != LevelManager.State.Alive) { return;}


        SoundManager.instance.boostSound.Stop();
        switch (collision.gameObject.tag) 
        {
            case "Friendly":
                break;
            case "Finish":
                StartWinSequence();
                break;
            case "FireFlyButt":
                break;
            default:
                ManageHits(collision.gameObject);
                break;            
        }
    }


    private void StartWinSequence()
    {
        LevelManager.state = LevelManager.State.Transcending;
        SoundManager.instance.WinSound();
        levelWonParticles.Play();
        Invoke("LoadNextScene", SoundManager.winSoundDuration);
    }

//    public void LoadNextScene()
//    {
//        //LevelManager.LoadNextScene(Enum.GetName(typeof(State), state));
//        LevelManager.LoadNextScene();
//

    public void ManageHits(GameObject tag)
    {
        print(tag.name);
        hits++;
        CheckIfDying();
        if (LevelManager.state != LevelManager.State.Dead)
        {
            HitObject();
        }
    }

    public void HitObject()
    {
        SoundManager.instance.CollisionSound();
        particleManager.HitObjectParticles(transform.position, transform.rotation, this.transform); 
    }

    public void CheckIfDying()
    {
        if (hits >= maxHits)
        {
            LevelManager.state = LevelManager.State.Dead;
            StartDeathSequence();
        }
    }

    private void StartDeathSequence()
    {
        SoundManager.instance.DyingSound();
        Invoke("LoadNextScene", SoundManager.dyingSoundDuration);
        StartCoroutine(DeathFlicker());
    }

    public IEnumerator DeathFlicker()
    {

        for (var i = 0; i < deathFlickers; i++)
        {
            fireFlyLight.enabled = true;
            yield return new WaitForSeconds(flickerSpeed);
            fireFlyLight.enabled = false;
            yield return new WaitForSeconds(flickerSpeed);
        }

    }


    public void LoadNextScene()
    {
        LevelManager.LoadNextScene();
    }


    /// <summary>
    /// AddReletiveForce applies force while isBoosting is true
    /// for the function that changes the isBoosting state,
    /// a burst of force on the initial call of the function counteracts the heavy
    /// steering when firefly takes off in a downward direction
    /// </summary>
   
    public void RespondToThrust() //new method to deal with GUI input
    {



        if (isBoosting)     //force relative to rotation
        {
            ThrustEffects();


            fireFlyRigidBody.AddRelativeForce(new Vector3(0f, Mathf.Abs(velocity), 0f));

        }
                                                      
            else
            {
                boostParticles.Stop();
                SoundManager.instance.boostSound.Stop();
            }


    }


    private void ThrustEffects() //todo if isThrusting, ten do this
    {
        if (!SoundManager.instance.boostSound.isPlaying)
        { SoundManager.instance.boostSound.Play(); }

        if (!boostParticles.isEmitting)
        { boostParticles.Play(); }

    }

    public void BoostPulse()
    {
        Vector3 directionalRoation = new Vector3(fireFlyRigidBody.rotation.x, fireFlyRigidBody.rotation.y, 0);
        //float velocityInDirection = Vector3.Dot(fireFlyRigidBody.velocity, direction);
        //print(Vector3.Project(fireFlyRigidBody.velocity, direction));//.magnitude);

        float direction;

        switch (rotation)
        {
            case Rotation.Left:
                direction = pulseForce * -1f;
                break;
            case Rotation.Right:
                direction = pulseForce * 1f;
                break;
            case Rotation.Static:
                direction = 0f;
                break;
            default:
                direction = directionalRoation.x;
                break;
        }

        Vector3 pulseDirection = new Vector3(direction, pulseForce, 0f);

        fireFlyRigidBody.AddForce(pulseDirection, ForceMode.Impulse);

    }

    /// <summary>
    /// Splitting rotation into 2 parts, sot hat I can call left and right seprately
    /// </summary>
    private void RotationDirection()

    { 
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            rotation = Rotation.Right;
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        { rotation = Rotation.Static; }


        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            rotation = Rotation.Left;
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            rotation = Rotation.Static;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            Boost();
        } else if (Input.GetKeyUp(KeyCode.Space)) { StopBoost(); }
    }

    

    public void RespondToRotate()
    {
        fireFlyRigidBody.angularVelocity = Vector3.zero; // remove rotation due to physics

        float rotationPerFrame = rotationalThrust * Time.deltaTime;

        Vector3 direction;

        if(rotation == Rotation.Right)
        { direction = -Vector3.forward; }
        else if(rotation == Rotation.Left)
        { direction = Vector3.forward; }
        else { return; }

        if (TiltFirefly(rotation))
        {
            transform.Rotate(direction * rotationPerFrame);
        } 
        
    }

    private void SkipLevel()
    {
        if (Input.GetKey(KeyCode.L))
        {
            LevelManager.state = LevelManager.State.Transcending;
            LoadNextScene();
        }
    }

    public void RightFirefly(Rotation rotation)
    {
        if (rotation == Rotation.Static)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, Time.deltaTime / selfRightingSpeed);


        }
    }



    private bool TiltFirefly(Rotation rotation)
    {
        float maxBackward = -0.4f;
        float maxForward = 0.4f;

        float currentTiltZAxis = transform.rotation.z;


        if (currentTiltZAxis > maxBackward && currentTiltZAxis < maxForward)
        {
            if (rotation == Rotation.Left && currentTiltZAxis > maxBackward)
            { return true; }
            else if (rotation == Rotation.Right && currentTiltZAxis < maxForward)
            { return true; }
        }
        return false;

    }



    /// <summary>
    /// TiltFirefly() legacy method
    /// returns a bool if the angle is with a range that it
    /// can accept an input
    /// Must be re-written to accept a target direction so as be more responsive
    /// </summary>
    /// <returns></returns>
//    private bool TiltFirefly()
//    {
//        float maxBackward = -0.4f;
//        float maxForward = 0.4f;
//        float currentTiltZAxis = transform.rotation.z;
//        
//
//        bool canTilt = (currentTiltZAxis > maxBackward && currentTiltZAxis < maxForward) ? true : false;
//        return canTilt;
//    }


    /// <summary>
    /// Fuctions to respond to Pointer up adn down event triggers
    /// </summary>
    /// 

    public void Boost()
    {
        //Add a pulse of force once when the boost is called
        BoostPulse();
        isBoosting = true;
    }

    public void StopBoost()
    {
        isBoosting = false;
    }

    public void ButtonRight()
    {
        rotation = Rotation.Right;  
    }

    public void ButtonLeft()
    {
        rotation = Rotation.Left;
    }

    public void Stabilize()
    {
        rotation = Rotation.Static;
    }
}
