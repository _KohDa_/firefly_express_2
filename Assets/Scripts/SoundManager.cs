﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class SoundManager : MonoBehaviour {

    public AudioMixer audioMixer;
    //public static float mixerVolume;

    public AudioSource dyingSound;
    public AudioSource soundEffects;
    public AudioSource boostSound;
    public AudioSource boulderSound;

    public AudioClip collisionsSound;
    public AudioClip winSound;
    public AudioClip powerUp;
    public AudioClip menuClick;
    public AudioClip boulderRelease;
    
    public static SoundManager instance; //Allows other scripts to call functions of SoundManager
    public static float dyingSoundDuration;
    public static float winSoundDuration;

    void Awake()
    {
       DontDetroyMe();
    }

    // Use this for initialization
    void Start () {
        dyingSoundDuration = dyingSound.clip.length;
        winSoundDuration = winSound.length;
        audioMixer.SetFloat("volumeEffects", VolumeManager.effectVolume);

    }
	
	// Update is called once per frame
	public void Update () {

    }

    private void DontDetroyMe()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this) //Destroy this, this enforces singleton pattern so there can only be one instance of SoundManager.
        {
            Destroy(gameObject);
        }

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void VolumeEffects(float volume)
    {
        audioMixer.SetFloat("volumeEffects", volume);
        VolumeManager.effectVolume = volume;
    }

    //doesnt' work
    public void VolumeEffectsTest(Slider slider)
    {
        if (slider.IsInvoking("VolumeEffects"))
        { soundEffects.PlayOneShot(winSound); }
        else if (!slider.IsInvoking("VolumeEffects")) { soundEffects.Stop(); }
    }

 
    public void DyingSound()
    {
        dyingSound.Play();
    }

    public void CollisionSound()
    {
        soundEffects.PlayOneShot(collisionsSound);
    }

    public void WinSound()
    {
        soundEffects.PlayOneShot(winSound);
    }

    public void MenuClickSound()
    {
        soundEffects.PlayOneShot(menuClick);
    }

    public void BoulderRelease()
    {
        boulderSound.PlayOneShot(boulderRelease);
    }

}
