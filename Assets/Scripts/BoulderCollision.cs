﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BoulderCollision : MonoBehaviour {

    private List<string> collisionObjects;
    private int maxCollisions;

    private AudioSource boulderAudio;

    public AudioClip releaseSound;
    public AudioClip collisionSound;

    // Use this for initialization
    void Start () {
        //collisionObjects = new string[50];
        collisionObjects = new List<string>();
        boulderAudio = gameObject.GetComponent<AudioSource>();
        boulderAudio.PlayOneShot(releaseSound, 1f);

            
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        string col = collision.gameObject.name;

        if(collisionObjects.Contains(col))
        {
            return;
        } else
        {
            collisionObjects.Add(col);
            boulderAudio.PlayOneShot(collisionSound);
        }

        
    }

}
