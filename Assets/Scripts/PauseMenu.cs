﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public GameObject pauseMenuUI;
    public GameObject inGameMenuUI;
    public GameObject optionsMenuUI;

    private int mainMenuIndex = 0;

    private SoundManager soundManager;

    private void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
    }


    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        inGameMenuUI.SetActive(true);
//        playerControlsUI.SetActive(true);
        Time.timeScale = 1f;
        LevelManager.gameIsPaused = false;
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        inGameMenuUI.SetActive(false);
//        playerControlsUI.SetActive(false);
        Time.timeScale = 0f;
        LevelManager.gameIsPaused = true;
    }

    public void Options()
    {
        optionsMenuUI.SetActive(true);
        pauseMenuUI.SetActive(false);
    }

    public void BackToPauseMenu()
    {
        optionsMenuUI.SetActive(false);
        pauseMenuUI.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenuIndex);
    }

    public void MenuClickSound()
    {
        soundManager.soundEffects.PlayOneShot(soundManager.powerUp);
    }
}
