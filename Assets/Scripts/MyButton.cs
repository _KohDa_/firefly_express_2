﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyButton : MonoBehaviour
{ 

    private Firefly firefly;
    private Canvas thisCanvas;

    private void Start()
    {
        firefly = FindObjectOfType<Firefly>();
        thisCanvas = this.gameObject.GetComponent<Canvas>();

    }

    private void Update()
    {

    }

    public void TurnOffForPause()
    {
        if (!LevelManager.gameIsPaused)
        { thisCanvas.enabled = true; }
        else { thisCanvas.enabled = false; }


    }

    public void Boost()
    {
        firefly.isBoosting = true;
    }

    public void Pulse()
    {
        firefly.BoostPulse();
    }

    public void StopBoost()
    {
        firefly.isBoosting = false;
    }

    public void ButtonRight()
    {
        Firefly.rotation = Firefly.Rotation.Right;
    }

    public void ButtonLeft()
    {
        Firefly.rotation = Firefly.Rotation.Left;
    }

    public void Stabilize()
    {
        Firefly.rotation = Firefly.Rotation.Static;
    }

    public void Rotate()
    {
        firefly.RespondToRotate();
    }

    public void GameStarted()
    {
        Ocilator.gameStarted = true;
    }
}
