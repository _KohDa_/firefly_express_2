﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {


    public Transform target;
    public float smoothSpeed = 0.125f; //higher value = faster locking (0 to 1)
    public Vector3 offset; //offset camera on all axes

    // Thogh Update seems intuative, if the target and the camera are being updated at the same time, 
    //this results itn the two competing, resulting in jittering.
    //late update is the same as Update, but runs just after, and FixedUpdate is called at the end of the physics loop

    private void FixedUpdate()
    {
        //smoothly follow target
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, t: Time.deltaTime * smoothSpeed );

        //camera position to vector of target plus vectors of the offset
        transform.position = smoothedPosition;

        //camera looks at target when offset is applied
        transform.LookAt(target);
    }

}
