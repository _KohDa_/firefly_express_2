﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonFlash : MonoBehaviour {

    [SerializeField] GameObject buttonGameObject;
    [SerializeField] float highAlphaValue = 1f;

    bool isFlashing = false;
    bool startDelayComplete = false;

    [SerializeField] float startDelayTime = 2.0f;
    [SerializeField] float lerpTime = 1.5f;
    public float currentLerpTime;

    Image buttonImage;


    Color defaultColor; 
    Color highAlphaColour;
    Color lerpedColor;


    // Use this for initialization
    void Start () {

    }
    

    // Update is called once per frame
    void OnEnable ()
    {
        buttonImage = buttonGameObject.gameObject.GetComponent<Image>();


        defaultColor = buttonImage.color;
        highAlphaColour = defaultColor;
        highAlphaColour.a = highAlphaValue;

        StartCoroutine("StartDelay");
    }

    IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(startDelayTime);
        startDelayComplete = true;
    }

    private void Update()
    {
        LerpColor();

    }


    public void LerpColor()
    {

        if (!startDelayComplete)
        { return; }


        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = 0f;
            if (isFlashing)
                { isFlashing = false; }
            else { isFlashing = true; }
        }

        float perc = currentLerpTime / lerpTime;

        if(!isFlashing)
        {
            lerpedColor = Color.Lerp(defaultColor, highAlphaColour, perc);

        } else if (isFlashing)
        {
            lerpedColor = Color.Lerp(highAlphaColour, defaultColor, perc);
        } else { print("neither flashing"); }
        
        buttonImage.color = lerpedColor;
    }





}
