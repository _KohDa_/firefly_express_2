﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ThemeMusic : MonoBehaviour {


    public AudioMixer audioMixer;
    //public static float mixerVolume;

    public AudioSource musicSource; // Reference to background music

    public static ThemeMusic themeMusic;

    // Use this for initialization


    private void Awake()
    {
        DontDestroyMe();
    }

    void Start ()
    {

        if (PlayerPrefsX.GetBool("MusicIsOn"))
        { ThemeMusicOn();
            VolumeMainMusic(VolumeManager.musicVolume);
        }
        else
        { ThemeMusicOff(); }
        //audioMixer.SetFloat(VolumeManager.volumeManager.themeMusicExposed, VolumeManager.musicVolume);

    }


    // Update is called once per frame
    void Update () {
		
	}

    void DontDestroyMe()
    {
        if (themeMusic == null)
        {
            themeMusic = this;
        }
        else if (themeMusic != this) //Destroy this, this enforces singleton pattern so there can only be one instance
        {
            Destroy(gameObject);
        }

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void ThemeMusicOn()
    {
        PlayerPrefsX.SetBool("MusicIsOn", true);
        if (musicSource.isPlaying)
        { return; }
        VolumeMainMusic(VolumeManager.musicVolume);
        musicSource.Play();
    }

    public void ThemeMusicOff()
    {
        PlayerPrefsX.SetBool("MusicIsOn", false);
        if (!musicSource.isPlaying)
        { return; }
        musicSource.Stop();
    }

    public void VolumeMainMusic(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        VolumeManager.musicVolume  = volume;
    }


}
