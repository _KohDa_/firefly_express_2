﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class VolumeManager : MonoBehaviour {

    public static VolumeManager volumeManager;

    public static float startVolumePerc = -0.5f;
    public static float startVolume;
    public static float musicVolume;
    public static float effectVolume;

    [SerializeField] public float audioMixerMin = -80.0f;
    [SerializeField] public float audioMixerMax = 0.0f;


    [SerializeField]
    public AudioMixer themeAudioMixer;

    [SerializeField]
    public string themeMusicExposed = "volume";

    [SerializeField]
    public AudioMixer effectsAudioMixer;

    [SerializeField]
    public string effectsMusicExposed = "volumeEffects";

    private void Awake()
    {
        //DontDestoyMe();
    }

    private void OnEnable()
    {

        startVolume = AdjustStartVolume();
        musicVolume = PlayerPrefs.GetFloat("MusicVolume", startVolume);
        effectVolume = PlayerPrefs.GetFloat("EffectsVolume", startVolume);

        //now hapens in the seperate managers
        //themeAudioMixer.SetFloat(themeMusicExposed,musicVolume);
        //effectsAudioMixer.SetFloat(effectsMusicExposed, effectVolume);


    }

    public float AdjustStartVolume()
    {
        float volumeRange = audioMixerMax - audioMixerMin;

        float startVolume = volumeRange * startVolumePerc;


        return startVolume;
    }

    public void DontDestoyMe()
    {

        if (volumeManager == null)
        {
            volumeManager = this;
        }
        else if (volumeManager != this) 
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
