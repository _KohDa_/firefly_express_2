﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIbars : MonoBehaviour {

    [SerializeField] Slider healthBar;
    private Firefly firefly;

	void Start () {
        firefly = FindObjectOfType<Firefly>();
        healthBar.maxValue = firefly.maxHits;
        healthBar.value = healthBar.maxValue;
	}

	void Update () {
        healthBar.value = healthBar.maxValue - Firefly.hits;
	}
}
