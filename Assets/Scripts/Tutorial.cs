﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tutorial : MonoBehaviour {


    [SerializeField] GameObject[] panels = new GameObject[6];
    [SerializeField] GameObject nextButtonGameObject;
    [SerializeField] GameObject playButtonGameObject;

    int currentPanel;
    int nextPanel;


    void Start () {
        currentPanel = 0;
        nextPanel = 1;
    }


    private void Update()
    {
    }



    public void NextPanel()
    { 
        panels[currentPanel].SetActive(false);
        panels[currentPanel +1].SetActive(true);

        currentPanel++;
        nextPanel++;
        if (nextPanel == panels.Length)
        {
            nextButtonGameObject.SetActive(false);
            playButtonGameObject.SetActive(true);
        }
    }
   

}
