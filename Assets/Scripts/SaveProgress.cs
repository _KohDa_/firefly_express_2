﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveProgress : MonoBehaviour {


    public static Vector3 originPos;
    public static Vector3 startPos;

    public static bool loadProgress;

    private void OnEnable()
    {
        SetOrigin();
        ClearProgress();

    }

    public void SaveSpot(Vector3 saveSpot)
    {
        loadProgress = true;
        startPos = saveSpot;

    }

    public void ClearProgress()
    {
        int startScreenIndex = 0;


        if (SceneManager.GetActiveScene().buildIndex == startScreenIndex || !loadProgress)
        {
            ResetStartPosition();
        } else if (loadProgress)
            { print("loadprogress is true and start position is :" + startPos); }

    }

    public void ResetStartPosition()
    {
        loadProgress = false;
        startPos = originPos;
    }

    public void SetOrigin()
    {
        GameObject fireFly = GameObject.FindGameObjectWithTag("Hero");
        bool sceneHasFF = (fireFly!= null) ? true : false;

        if(sceneHasFF)
        {
            originPos = fireFly.transform.position;
        }
        else { originPos = this.transform.position; }
    }
}
