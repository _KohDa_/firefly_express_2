﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelSelector : MonoBehaviour {

    [SerializeField] Button[] levelButtons;

    //[SerializeField] TMP_ColorGradient offGradient;
    //[SerializeField] TMP_ColorGradient onGradient;
    [SerializeField] Color disabledTextColour;

    [SerializeField] Color enabledTextColour;



    void Start () {
		
	}

    private void OnEnable()
    {
        SetLevelsScreen();
    }

    public void SetLevelsScreen()
    {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            if (i + 1 > PlayerPrefs.GetInt("MaxLevel"))
            {
                Text thisText = levelButtons[i].GetComponentInChildren<Text>();
                thisText.color = disabledTextColour;
                levelButtons[i].interactable = false;
            }
            else
            {
                Text thisText = levelButtons[i].GetComponentInChildren<Text>();
                thisText.color = enabledTextColour;
                levelButtons[i].interactable = true;
            }
        }
    }

    public void SelectLevel(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }

    public void ResetProgress()
    {
        PlayerPrefs.SetInt("MaxLevel", 1);
        //SetLevelsScreen();
        print(PlayerPrefs.GetInt("MaxLevel"));
    }

    /// <summary>
    /// just for snadboxing
    /// </summary>
    public void AddLevel()
    {
        int addlevel = PlayerPrefs.GetInt("MaxLevel") +1;
        PlayerPrefs.SetInt("MaxLevel", addlevel);
        SetLevelsScreen();
        print(PlayerPrefs.GetInt("MaxLevel"));
    }
}
