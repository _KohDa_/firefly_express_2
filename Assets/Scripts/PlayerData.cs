﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{

    public string Name { get; set; }
    public int Health { get; set; }
    public int MaxLevel { get; set; }
    public float MusicVolume { get; set; }
    public float EffectsVolume { get; set; }
    public bool MusicIsOn { get; set; }

}
