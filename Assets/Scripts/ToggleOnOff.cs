﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ToggleOnOff : MonoBehaviour {


    private UnityEngine.UI.Toggle toggle;
    private Image image;
    [SerializeField] Sprite onButton;
    [SerializeField] Color onColor;
    [SerializeField] Sprite offButton;
    [SerializeField] Color offColor;

    Image onImage, offImage;

    ColorBlock colorTint;



    private void Start()
    {

        toggle = GetComponent<Toggle>();
        image = GetComponent<Image>();
        toggle.onValueChanged.AddListener(OnToggleValueChanged);

        //colorTint = toggle.colors;


        bool musicPlaying = PlayerPrefsX.GetBool("MusicIsOn");


        if (musicPlaying)
        { OffImage(); }
        else
        { OnImage(); }

        toggle.isOn = (musicPlaying) ? true : false;




    }

    public void OnToggleValueChanged(bool isOn)
    {
        if(!isOn)
        {
            OnImage();
            ThemeMusic.themeMusic.ThemeMusicOff();
        }
        else if (isOn)
        {
            OffImage();
            ThemeMusic.themeMusic.ThemeMusicOn();
        }

        //toggle.transition = Selectable.Transition.ColorTint;
        //toggle.colors = colorTint;

    }

    private void OnImage()
    {
        image.sprite = onButton;
        image.color = onColor;
    }

    private void OffImage()
    {
        image.sprite = offButton;
        image.color = offColor;
    }


///    private void OnToggleValueChanged(bool isOn)
    //    {
    //        TextMeshProUGUI thisTextMesh = GetComponentInChildren<TextMeshProUGUI>();
    //        if (!isOn)
    //        {
    //            thisTextMesh.colorGradientPreset = offGradient;
    //
    //        }
    //        else if (isOn) 
    //        {
    //            thisTextMesh.colorGradientPreset = onGradient;
    //        }
    //    }
}
