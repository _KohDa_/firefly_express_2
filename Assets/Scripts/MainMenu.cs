﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour {

    private SoundManager soundManager;
    private int tutorialSceneIndex;


    private void Start()
    {
        tutorialSceneIndex = SceneManager.sceneCountInBuildSettings - 1;
        soundManager = FindObjectOfType<SoundManager>();

        //delete mid-level progress if this scene is the start menu
        FindObjectOfType<SaveProgress>().ClearProgress();


    }

    public void CheckContinue()
    {
       
    }

    public void ResumeGame()
    {
        SceneManager.LoadScene(PlayerPrefs.GetInt("MaxLevel"));
    }

    public void StartGame(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void RunTutorial()
    {
        SceneManager.LoadScene(tutorialSceneIndex);
    }

    public void MenuClickSound()
    {
        soundManager.MenuClickSound();
    }

    public void StartGameSound()
    {
        soundManager.soundEffects.PlayOneShot(soundManager.winSound);
    }
}
