﻿using UnityEngine;
using UnityEngine.UI;

public class VolumeSliders : MonoBehaviour
{


    private VolumeManager volumeManager;
    private ThemeMusic themeMusic;
    private SoundManager soundManager;

    public Slider themeMusicSlider;
    public Slider effectMusicSlider;

    public static float themeMusicSliderVolume;



    // Use this for initialization
    void Start()
    {

        volumeManager = FindObjectOfType<VolumeManager>();

        SetRange(themeMusicSlider);
        SetRange(effectMusicSlider);

    }



    private void OnEnable()
    {


        themeMusic = FindObjectOfType<ThemeMusic>();
        soundManager = FindObjectOfType<SoundManager>();
        if (this.enabled && themeMusic.enabled)
        {
            themeMusicSlider.value = VolumeManager.musicVolume;
            effectMusicSlider.value = VolumeManager.effectVolume;
        }
        else { return; }


    }


    public void SetRange(Slider slider)
    {
        slider.minValue = volumeManager.audioMixerMin;
        slider.maxValue = volumeManager.audioMixerMax;
    }

    public int UpdatePlayerPrefs(float volume)
    {

        int volumeInt = (int)volume;

        return volumeInt;
    }

    public void MusicPlay()
    {
        themeMusic.ThemeMusicOn();
    }

    public void MusicStop()
    {
        themeMusic.ThemeMusicOff();
    }

    public void MusicVolume(float musicVolume)
    {
        themeMusic.VolumeMainMusic(musicVolume);
        VolumeManager.musicVolume = musicVolume;
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
    }

    public void EffectsVolume(float effectsVolume)
    {
        soundManager.VolumeEffects(effectsVolume);
        VolumeManager.effectVolume = effectsVolume;
        PlayTest();
        PlayerPrefs.SetFloat("EffectsVolume", effectsVolume);
    }

    public void PlayTest()
    { soundManager.soundEffects.PlayOneShot(soundManager.powerUp); }

}
