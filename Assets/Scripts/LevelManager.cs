﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


//namespace isnt' required
//namespace FireFlyExpress

    public class LevelManager : MonoBehaviour {

        public static bool gameIsPaused = false;
        public static LevelManager levelManager;
        public static int currentSceneIndex;
        public static int nextSceneIndex;

        public enum State { Alive, Dead, Transcending };
        public static State state;


        // Use this for initialization
        void Start()
        {

            currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            nextSceneIndex = currentSceneIndex + 1;

        }

        // Update is called once per frame
        void Update()
        {
        }


        public void ButtonClicked()
        { print("Button Clicked"); }

        public static void LoadNextScene()
        {


            switch (state)
            {
                case State.Alive:
                    //n/a
                    break;
                case State.Dead:
                    SceneManager.LoadScene(currentSceneIndex);
                    break;
                case State.Transcending:
                    SetHighestLevel();
                    FindObjectOfType<SaveProgress>().ResetStartPosition();
                if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
                    { SceneManager.LoadScene(0); }
                    else { SceneManager.LoadScene(nextSceneIndex); }
                    break;
            }



        }

        public static void SetHighestLevel()
        {
            int winScene = 1;
            int highestLevel = SceneManager.sceneCountInBuildSettings - winScene;
            int storedLevel = PlayerPrefs.GetInt("maxLevel");
            if (nextSceneIndex > storedLevel && nextSceneIndex < highestLevel)
            { PlayerPrefs.SetInt("MaxLevel", nextSceneIndex); }


        }
    }

